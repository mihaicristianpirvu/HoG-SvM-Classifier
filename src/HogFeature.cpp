#include "HogFeature.hpp"

const constexpr std::tuple<std::size_t, std::size_t> HogFeature::blocks_shape;
const constexpr std::tuple<std::size_t, std::size_t> HogFeature::cells_per_block;

HogFeature::HogFeature(const Image<double> &image) : image(image), magnitude(image.getHeight(), image.getWidth()),
	orientation_bin(image.getHeight(), image.getWidth()) {
	//std::cerr << "[HogFeature] Computing HoG for " << image << "\n";
	static_assert(HogFeature::histogram_degrees == 180 || HogFeature::histogram_degrees == 360, "");
	this->computeMagnitudeAndOrientation();
	this->computeCellsHistogram();
	this->computeBlockDescriptor();
}

void HogFeature::computeMagnitudeAndOrientation() {
	//std::cerr << "[HogFeature::computeMagnitudeAndOrientation]\n";
	/* Initialize the vector with w * h values of 0 */
	std::size_t width = this->image.getWidth(), height = this->image.getHeight();
	double gradientX, gradientY;

	/* The very edges must be done manually so we can have the other fors common for Gx and Gy. */
	std::tie(this->magnitude(0, 0), this->orientation_bin(0, 0)) = this->getMagnitudeAndOrientation(0, 0);
	std::tie(this->magnitude(0, width - 1), this->orientation_bin(0, width - 1)) =
		this->getMagnitudeAndOrientation(0, 0);
	std::tie(this->magnitude(height - 1, 0), this->orientation_bin(height - 1, 0)) =
		this->getMagnitudeAndOrientation(0, 0);
	std::tie(this->magnitude(height - 1, width - 1), this->orientation_bin(height - 1, width - 1)) =
		this->getMagnitudeAndOrientation(0, 0);

	for(std::size_t i = 1; i < height - 2; i++) {
		gradientX = 0;
		gradientY = this->image(i + 1, 0) - this->image(i - 1, 0);
		std::tie(this->magnitude(i, 0), this->orientation_bin(i, 0)) =
			this->getMagnitudeAndOrientation(gradientX, gradientY);
		gradientY = this->image(i + 1, width - 1) - this->image(i - 1, width - 1);
		std::tie(this->magnitude(i, width - 1), this->orientation_bin(i, width - 1)) =
			this->getMagnitudeAndOrientation(gradientX, gradientY);
	}

	for(std::size_t j = 1; j < width - 2; j++) {
		gradientY = 0;
		gradientX = this->image(0, j + 1) - this->image(0, j - 1);
		std::tie(this->magnitude(0, j), this->orientation_bin(0, j)) =
			this->getMagnitudeAndOrientation(gradientX, gradientY);
		gradientX = this->image(height - 1, j + 1) - this->image(height - 1, j - 1);
		std::tie(this->magnitude(height - 1, j), this->orientation_bin(height - 1, j)) =
			this->getMagnitudeAndOrientation(gradientX, gradientY);
	}

	/* The rest of the matrix is generated applying [-1, 0, 1] kernel. */
	for(std::size_t i = 1; i < height - 1; i++) {
		for(std::size_t j = 1; j < width - 1; j++) {
			/* [i][j] = [i][j+1] - [i][j-1] */
			/* [i][j] = [i + 1][j] - [i - 1][j] */
			gradientX = (this->image(i, j + 1) - this->image(i, j - 1));
			gradientY = (this->image(i + 1, j) - this->image(i - 1, j));
			std::tie(this->magnitude(i, j), this->orientation_bin(i, j)) =
				this->getMagnitudeAndOrientation(gradientX, gradientY);
		}
	}
}

std::tuple<double, std::uint8_t> HogFeature::getMagnitudeAndOrientation(double gradientX, double gradientY, bool show) {
	double magnitude, orientation;
	std::uint8_t bin;

	magnitude = std::hypot(gradientX, gradientY);
	/* Atan2 = [-pi, pi] => Atan2 + pi = [0, 2 * pi] */
	orientation = (std::atan2(gradientY, gradientX) + M_PI) * 180 / M_PI;

	/* This is python's equivalent to orientation % 180 for doubles (do %180 for int and add the value after decimal) */
	if(HogFeature::histogram_degrees == 180)
		orientation = (int)orientation % 180 + (orientation - (int)orientation);

	/* 36 / (180/9) = 36 / 15 = 2 => bin 2 */
	bin = orientation / (HogFeature::histogram_degrees / HogFeature::number_of_orientations);
	assert(bin >= 0 && bin < HogFeature::number_of_orientations);

	return std::make_tuple(magnitude, bin);
}

void HogFeature::computeCellsHistogram() {
	for(std::size_t cell_index = 0; cell_index < 16 * 8; ++cell_index) {
		std::size_t cell_i = cell_index / 8;
		std::size_t cell_j = cell_index % 8;
		for(std::size_t i = cell_i * 8; i < (cell_i + 1) * 8; ++i) {
			for(std::size_t j = cell_j * 8; j < (cell_j + 1) * 8; ++j) {
				std::uint8_t bin = this->orientation_bin(i,j);
				assert(bin >= 0 && bin < number_of_orientations);
				this->cells_histogram[cell_i][cell_j][bin] += this->magnitude(i, j);
				this->cell_histogram_sum[cell_i][cell_j] += this->magnitude(i, j);
			}
		}
	}
}

void HogFeature::computeBlockDescriptor() {
	//std::cerr << "[HogFeature::computeDescriptor] Computing a descriptor of size: " << descriptor_size << "\n";

	for(std::size_t block_index = 0; block_index < std::get<I>(blocks_shape) * std::get<J>(blocks_shape);
		++block_index) {
		std::size_t block_i = block_index / (std::get<J>(blocks_shape));
		std::size_t block_j = block_index % (std::get<J>(blocks_shape));
		std::size_t descriptor_base_offset = block_index * std::get<I>(cells_per_block) * std::get<J>(cells_per_block)
			* number_of_orientations;

		/* TODO: use integral image for sum of a block. */
		double block_sum = 0;
		for(std::size_t cell_index = 0; cell_index < std::get<J>(cells_per_block) * std::get<I>(cells_per_block);
			++cell_index) {
			std::size_t cell_i = block_i + (cell_index / std::get<J>(cells_per_block));
			std::size_t cell_j = block_j + (cell_index % std::get<J>(cells_per_block));
			block_sum += cell_histogram_sum[cell_i][cell_j];
		}
		block_sum = std::sqrt(block_sum * block_sum + 1e-5);

		/* For each block, compute the descriptor based on its corresponding cells */
		for(std::size_t cell_index = 0; cell_index < std::get<J>(cells_per_block) * std::get<I>(cells_per_block);
			++cell_index) {
			std::size_t cell_i = block_i + (cell_index / std::get<J>(cells_per_block));
			std::size_t cell_j = block_j + (cell_index % std::get<J>(cells_per_block));
			assert(cell_i >= 0 && cell_i < 16 && cell_j >= 0 && cell_j < 8);

			std::size_t descriptor_offest = descriptor_base_offset + (cell_index * number_of_orientations);
			for(std::size_t orientation_index = 0; orientation_index < number_of_orientations; ++orientation_index)
				descriptor[descriptor_offest + orientation_index] =
					cells_histogram[cell_i][cell_j][orientation_index] / block_sum;
		}
	}
}
