#include "DatasetMeta.hpp"
#include "HogFeature.hpp"
#include "Image.hpp"
#include <cassert>
#include <iostream>
#include <string>
#include <vector>
#include <ArgParser.hpp>
#include <opencv2/imgcodecs.hpp>

using namespace std::string_literals;

static void parseArgs(ArgParser &parser) {
	parser.setUsage("Usage: ./HoGSvM test, train or specific image\n"
		"\ttrain: --train --export=<path_to_export>\n"
		"\ttest: --test --import=<path_to_import>\n"
		"\timage: --image=<path_to_image>");

	parser.addArgument(Argument("train", "Enables training mode"), ArgType::OPTIONAL);
	parser.addArgument(ValuedArgument<>("export", "Path to export the SVM"), ArgType::OPTIONAL);
	parser.addArgument(Argument("test", "Enables testing mode"), ArgType::OPTIONAL);
	parser.addArgument(ValuedArgument<>("import", "Path to import the SVM"), ArgType::OPTIONAL);
	parser.addArgument(ValuedArgument<>("image", "Path to import just one image"), ArgType::OPTIONAL);
	parser.parseArguments();

	assert( ((parser.isSet("train") && parser.isSet("export")) || (parser.isSet("test") && parser.isSet("import"))
		|| parser.isSet("image")) && "Wrong parameters were set." );
}

int main(int argc, char **argv) {
	ArgParser parser(argc, argv);
	parseArgs(parser);

	assert(!parser.isSet("train") && !parser.isSet("test") && parser.isSet("image") && "NYI");

	if(parser.isSet("image")) {
		auto path = parser.getValue<std::string>("image");
		auto cvImage = cv::imread(path, cv::ImreadModes::IMREAD_GRAYSCALE);
		Image<double> image = Image<double>(cvImage);

		//for(std::size_t i = 0; i < 10; i++)
		//	std::cerr << image(i, 0) << "\n";
		//std::cerr << "--\n";

		HogFeature hog(image);

		for(std::size_t i = 0; i < 10; i++)
			std::cerr << hog.descriptor[i] << "\n";
	}

	return 0;
}