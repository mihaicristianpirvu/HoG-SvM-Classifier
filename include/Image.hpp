/* Image.hpp - Generic class for images and convert operators for various image processing libraries */
#ifndef IMAGE_HPP
#define IMAGE_HPP

#include <cassert>
#include <cstdint>
#include <cstring>
#include <iostream>
#include <type_traits>
#include <tuple>
#include <vector>
//#include <NumpyArray.h>
#include <opencv2/imgcodecs.hpp> /* From cv::Mat to std::vector */

template <typename T=std::uint8_t>
struct Image {
	Image() = delete;
	/* Only allocates the image without setting any value to the allocated memory */
	Image(std::size_t, std::size_t, std::size_t = 1);
	/* Create new image of w * h shape and assign the entire image to a value */
	Image(const T &, std::size_t, std::size_t, std::size_t = 1);
	/* Regular pointer compatibility */
	Image(const T *, std::size_t, std::size_t, std::size_t = 1);
	/* std::vector compatbility */
	Image(const std::vector<T> &, std::size_t, std::size_t, std::size_t = 1);
	/* Compatibility with NumpyArray */
	//Image(const NumpyArray *);
	/* OpenCV compatibility. TODO: Add define if opencv is detected. */
	Image(const cv::Mat &);
	/* Copy constructor */
	Image(const Image &);
	/* Move constructor */
	Image(Image &&);
	/* Destructor, free the allocated memory */
	~Image();

	/* Overload for image access (h, w), instead of using getter/setters. */
	const T &operator()(std::size_t, std::size_t, std::size_t = 0) const;
	T &operator()(std::size_t, std::size_t, std::size_t = 0);

	T *getImage() const;
	const std::size_t getWidth() const;
	const std::size_t getHeight() const;
	const std::size_t getDepth() const;

	/* 2D and 3D printing */
#if 0
	void printWindow(const std::tuple<std::size_t, std::size_t> &, const std::tuple<std::size_t, std::size_t> &) const;
	void printWindow(const std::tuple<std::size_t, std::size_t, std::size_t> &,
		const std::tuple<std::size_t, std::size_t, std::size_t> &) const;
#endif

private:
	T *image = nullptr;
	const std::size_t height, width, depth;
};

/* Used for std::cout/cerr printing */
template <typename T>
std::ostream &operator<<(std::ostream &os, const Image<T> &image)
{
	os << "Image (" << image.getHeight() << ", " << image.getWidth() << ", " << image.getDepth() << ")";
	return os;
}

template <typename T>
inline
Image<T>::Image(std::size_t height, std::size_t width, std::size_t depth) : height(height), width(width),
	depth(depth) {
	this->image = new T[height * width * depth];
	assert(this->image != nullptr);
}

template <typename T>
inline
Image<T>::Image(const T &value, std::size_t height, std::size_t width, std::size_t depth)
	: Image<T>(height, width, depth) {
	for(std::size_t i = 0; i < height * width * depth; ++i)
		this->image[i] = value;
}

template <typename T>
inline
Image<T>::Image(const T *other, std::size_t height, std::size_t width, std::size_t depth)
	: Image<T>(height, width, depth) {
	/* Copy the data manually, becuase the data types may be different. */
	assert(other != nullptr);
	for(std::size_t i = 0; i < width * height * depth; ++i) {
		this->image[i] = other[i];
	}
}

template <typename T>
inline
Image<T>::Image(const std::vector<T> &other, std::size_t height, std::size_t width, std::size_t depth)
	: Image<T>(other.data(), height, width, depth) {}

#if 0
template <typename T>
inline
Image<T>::Image(const NumpyArray *npArray) : Image<T>(npArray->shape[0], npArray->shape[1],
	npArray->shapeSize == 2 ? 1 : npArray->shape[2]) {
	assert(npArray->shapeSize == 2 || npArray->shapeSize == 3);
	/* Due to the fact that python will recall that npArray's memory, we need to copy it to use it. */
	std::memcpy(this->image, npArray->data, this->height * this->width * this->depth * sizeof(double));
}
#endif

template <typename T>
inline
Image<T>::Image(const cv::Mat &other) : Image<T>(other.rows, other.cols) {
	assert(other.isContinuous() && "Image must be continuous for now.");
	for(std::size_t i = 0 ; i < height * width; ++i) {
		this->image[i] = other.datastart[i];
	}
}

template <typename T>
inline
Image<T>::Image(const Image<T> &other) : Image<T>(other.getImage(), other.getHeight(), other.getWidth(),
	other.getDepth()) {}

template <typename T>
inline
Image<T>::Image(Image<T> &&other) : height(other.height), width(other.width), depth(other.depth) {
	this->image = other.image;
	other.image = nullptr;
}

template <typename T>
inline
Image<T>::~Image() {
	if(this->image != nullptr)
		delete this->image;
	this->image = nullptr;
}

template <typename T>
inline
const T &Image<T>::operator()(std::size_t i, std::size_t j, std::size_t k) const {
	assert(i < this->height && j < this->width && k < this->depth);
	return this->image[i * this->width * this->depth + j * this->depth + k];
}

template <typename T>
inline
T &Image<T>::operator()(std::size_t i, std::size_t j, std::size_t k) {
	assert(i < this->height && j < this->width);
	return this->image[i * this->width * this->depth + j * this->depth + k];
}

template <typename T>
inline
const std::size_t Image<T>::getWidth() const {
	return this->width;
}

template <typename T>
inline
const std::size_t Image<T>::getHeight() const {
	return this->height;
}

template <typename T>
inline
const std::size_t Image<T>::getDepth() const {
	return this->depth;
}

template <typename T>
inline
T *Image<T>::getImage() const {
	return this->image;
}

#if 0
template <typename T>
inline
void Image<T>::printWindow(const std::tuple<std::size_t, std::size_t> &topLeft, 
	const std::tuple<std::size_t, std::size_t> &bottomRight) const {
	std::size_t topLeft_i, topLeft_j, bottomRight_i, bottomRight_j;

	std::tie(topLeft_i, topLeft_j) = topLeft;
	std::tie(bottomRight_i, bottomRight_j) = bottomRight;
	assert(topLeft_i <= bottomRight_i && topLeft_j <= bottomRight_j);
	assert(topLeft >= std::make_tuple(0, 0) && topLeft < std::make_tuple(height, width));
	assert(bottomRight >= std::make_tuple(0, 0) && bottomRight < std::make_tuple(height, width));

	std::cerr << "Printing window: (" << topLeft_i << ", " << topLeft_j << "), (" << bottomRight_i << ", " <<
		bottomRight_j << ")\n";

	std::cerr << "{\n";
	for(std::size_t i = topLeft_i; i < bottomRight_i; i++) {
		std::cerr << "[ ";
		for(std::size_t j = topLeft_j; j < bottomRight_j; j++) {
			if(this->)
			std::cerr << (float)this->operator()(i, j) << " ";
		}
		std::cerr << "]\n";
	}
	std::cerr << "}";
}
#endif

#endif /* IMAGE_HPP */