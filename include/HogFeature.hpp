#ifndef HOG_FEATURE_HPP
#define HOG_FEATURE_HPP

#include "Image.hpp"
#include <array>
#include <cassert>
#include <cmath>
#include <iostream>
#include <tuple>

static const constexpr size_t I = 0, J = 1;

struct HogFeature {
	HogFeature(const Image<double> &image);

private:
	/* Paramters used by HoG */
	static const constexpr std::size_t number_of_orientations = 9;
	static const constexpr std::tuple<std::size_t, std::size_t> cell_shape = {8, 8}; /* (8, 8) pixels per cell */
	static const constexpr std::tuple<std::size_t, std::size_t> cells_per_block = {2, 2}; /* block shape */
	static const constexpr std::size_t histogram_degrees = 180; /* 180 or 360 */
	/* shape of the descriptor window */
	static const constexpr std::tuple<std::size_t, std::size_t> window_size = {128, 64}; 
	/* (128 / 8, 64 / 8) = (16, 8) */
	static const constexpr std::tuple<std::size_t, std::size_t> cells_count = {
		std::get<I>(window_size) / std::get<I>(cell_shape), std::get<J>(window_size) / std::get<J>(cell_shape) };
	/* {(16 - 1), (7 - 1)} = {15, 7} */
	static const constexpr std::tuple<std::size_t, std::size_t> blocks_shape = {
		std::get<I>(cells_count) - std::get<I>(cells_per_block) + 1,
		std::get<J>(cells_count) - std::get<J>(cells_per_block) + 1 };
	/* 2 * 2 * 9 * 15 * 7 = 3780 */
	static const constexpr std::size_t descriptor_size =
		std::get<I>(cells_per_block) * std::get<J>(cells_per_block) * number_of_orientations *
		std::get<I>(blocks_shape) * std::get<J>(blocks_shape);

	/* Compute the magnitude and orientation by the means of gradients and save it in the two class member images */
	void computeMagnitudeAndOrientation();
	/* Given Gx and Gy, compute the magnitude and orientation */
	std::tuple<double, std::uint8_t> getMagnitudeAndOrientation(double, double, bool=false);
	/* For each cell in the image, compute the histogram. */
	void computeCellsHistogram();
	/* After the cell histogram is computed, compute the block */
	void computeBlockDescriptor();

public:
	Image<double> image, magnitude;
	Image<std::uint8_t> orientation_bin;
	/* 16 x 8 x 9 */
	double cells_histogram[std::get<I>(cells_count)][std::get<J>(cells_count)][number_of_orientations] = {0};
	/* 16 x 8 */
	double cell_histogram_sum[std::get<I>(cells_count)][std::get<J>(cells_count)] = {0};
	double descriptor[descriptor_size] = {0};
};

#endif /* HOG_FEATURE_HPP */