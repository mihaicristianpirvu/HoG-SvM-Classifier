/* File used for different constants used in the project */
#ifndef CONSTANTS_HPP
#define CONSTANTS_HPP

#include "DatasetMeta.hpp"
#include "Image.hpp"
#include <iostream>
#include <map>
#include <string>
#include <vector>
#include <dirent.h>

/* Given a path, store the path of all files from that directory in the files variable */
void getFileList(const std::string &path, std::vector<std::string> &files) {
	DIR *dir;
	struct dirent *ent;

	using namespace std::string_literals;

	dir = opendir (path.c_str());
	ASSERT(dir != NULL, "Path "s + path + " not found"s);
	while( (ent = readdir (dir)) != NULL ) {
		if(strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0)
			continue;
		files.push_back(path + std::string(ent->d_name));
	}

	closedir(dir);
}

void readDataSet(const DatasetMeta &datasetMeta, std::map<std::string, std::vector<Image<float>>> &dataset) {
	using namespace std::string_literals;
	ASSERT(datasetMeta.classes.size() == datasetMeta.paths.size(), "Wrong number of paths vs classes");

	std::string result = "Read a dataset (" + datasetMeta.name + ") of: { ";

	for(std::size_t i=0; i<datasetMeta.classes.size(); ++i) {
		std::vector<std::string> files;
		auto &path = datasetMeta.paths[i];
		auto &class_type = datasetMeta.classes[i];
		getFileList(path, files);

		for(auto &file : files) {
			cv::Mat img = cv::imread(file, cv::ImreadModes::IMREAD_COLOR);
			ASSERT(!img.empty(), "Image not found: "s + file);
			dataset[class_type].push_back(Image<float>(img));
		}
		result += class_type + ": " + std::to_string(dataset[class_type].size()) + " ";
	}

	result += "}\n";
	std::cerr << result;
}

#endif /* CONSTANTS_HPP */