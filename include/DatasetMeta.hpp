/* Class that defines the meta informations about a dataset, needed to read, such as classes and paths for different
 * classes */
#ifndef DATASET_META
#define DATASET_META

#include <string>
#include <vector>

struct DatasetMeta {
	DatasetMeta(const std::string & = {}, const std::vector<std::string> & = {},
		const std::vector<std::string> & = {});
	void addClass(const std::string &, const std::string &);

	std::string name;
	std::vector<std::string> classes;
	std::vector<std::string> paths;
};

#endif /* DATASET_META */